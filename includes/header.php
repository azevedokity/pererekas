<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <title>Pererekas </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pererekas - salgadinhos e pipocas">
    <meta name="keywords" content="salgadinhos, pipocas">
    <meta name="author" content="Inovatech">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="images/icone.ico">

    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- ICONS -->
    <!-- Font Awesome 4 -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0/css/v4-shims.min.css" integrity="sha512-p++g4gkFY8DBqLItjIfuKJPFvTPqcg2FzOns2BNaltwoCOrXMqRIOqgWqWEvuqsj/3aVdgoEo2Y7X6SomTfUPA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Font awesome 5 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-2/css/all.min.css" integrity="sha512-61a6zi50gYXGgd/n9+ZT2/RKnXr6lkRoWlS88AjFdoUHaIDnyBAcoE0Vs/QDU3lK3nCcUowNDqmQ8WaV0yT4qw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="css/ilmosys-icon.css">
    <link rel="stylesheet" href="js/vendors/swipebox/css/swipebox.min.css">


    <!-- THEME / PLUGIN CSS -->
    <link rel="stylesheet" href="js/vendors/slick/slick.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/tiago.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- Recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>


    <!-- Tiago Scripts -->
    <script src="js/links_ativos.js"></script>

</head>




<body id="home">
    <div class="body">
        <!-- HEADER -->
        <header>
            <nav class="navbar-inverse navbar-lg navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="#home" class="navbar-brand brand page-scroll">
                            <img src="images/logo-07.png" alt="Logotipo Pererecas">
                        </a>
                    </div>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				    <span class="sr-only">Menu</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    </button>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right navbar-login">
                            <li>
                                 <a href="mailto:contato@pererekas.com.br"><span class="icon-call"></span> Fale conosco!</a>
                                 <!-- <a href="mailto:pipokaspererekas2@gmail.com.br"><span class="icon-call"></span> Fale conosco!</a> -->
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown mm-menu">
                                <a class="page-scroll" id="inicio" href="#home">Pererekas</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" id="sobre" href="#about">Sobre Nós</a>
                            </li>
							
							<li class="dropdown mm-menu">
                                <a class="page-scroll" id="produtos" href="#explore">Produtos</a>
                            </li>
                            
                            <li class="dropdown mm-menu">
                                <a class="page-scroll" id="peca-agora" href="#faq">Peça Agora</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>



