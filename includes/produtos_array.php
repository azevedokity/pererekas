<?php

  $produtos = array(

    array(
      'titulo' => 'CEBOLA E SALSA',
      'src' => 'images/produtos/salgadinho_cebolasalsa.jpg',
      'alt' => 'Pacote de Pererekas salgadinho sabor cebola e salsa'
    ),  

    array(
      'titulo' => 'BACON',
      'src' => 'images/produtos/salgadinho_bacon.jpg',
      'alt' => 'Pacote de Pererekas salgadinho sabor bacon'
    ),  

    array(
      'titulo' => 'PIZZA',
      'src' => 'images/produtos/salgadinho_pizza.jpg',
      'alt' => 'Pacote de Pererekas salgadinho sabor requeijão'
    ),  

    array(
      'titulo' => 'QUEIJO',
      'src' => 'images/produtos/salgadinho_queijo.jpg',
      'alt' => 'Pacote de Pererekas salgadinho sabor queijo'
    ),  

    array(
      'titulo' => 'PARMESÃO',
      'src' => 'images/produtos/salgadinho_parmesao.jpg',
      'alt' => 'Pacote de Pererekas salgadinho sabor parmesão'
    ),  

    array(
      'titulo' => 'REQUEIJÃO',
      'src' => 'images/produtos/salgadinho_requeijao.jpg',
      'alt' => 'Pacote de Pererekas pipocas sabor tutti-frutti'
    ),

    array(
      'titulo' => 'CHURRASCO',
      'src' => 'images/produtos/salgadinho_churrasco.jpg',
      'alt' => 'Pacote de Pererekas pipocas sabor tutti-frutti'
    ), 

    array(
      'titulo' => 'PIPOCA NATURAL',
      'src' => 'images/produtos/pipoca_natural.jpg',
      'alt' => 'Pacote de Pererekas pipocas sabor tutti-frutti'
    ), 

    array(
      'titulo' => 'PIPOCA TUTTI FRUTTI',
      'src' => 'images/produtos/pipoca_tutti_frutti.jpg',
      'alt' => 'Pacote de Pererekas pipocas sabor tutti-frutti'
    ), 


  );


?>
