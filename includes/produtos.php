<?php include('includes/produtos_array.php') ?>


<div id="explore" class="elements-content">
  <div class="container">
    <div class="about-inline text-center">
      <h3>Alguns de nossos deliciosos produtos </h3>
      <p>Além do sabor incomparável, os produtos Pererekas são todos produzidos com o maior cuidado e carinho levando ao consumidor final um produto de alta qualidade. Para todos os momentos, salgadinhos e pipocas é Pererekas®️.  E se no seu estabelecimento ainda não tem Pererekas, não perca tempo! Basta preencher e enviar o formulário abaixo de produtos que entraremos em contato.</p>
    </div>
    <div class="row">

      <?php 
      
        foreach ($produtos as $produto) { ?>
          <div class="col-md-4">
            <div class="card card-product">
              <h4 class="title"><?= $produto['titulo'] ?></h4>
              <div class="img-wrap">
                <img src="<?= $produto['src'] ?>" alt="<?= $produto['alt'] ?>"></div>
              <div class="info-wrap">
              </div>
            </div>
          </div> <?php
        } 
      
      ?>

    </div>
  </div>
</div>

<div class="space80"></div>
<div class="video-action">
  <div class="row text-center">
    <div class="intro-video-pop"></div>
  </div>
</div>

