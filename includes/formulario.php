<!-- Formulario -->
<section class="container" id="faq" >
  <div class="about-inline text-center">
    <h3>Solicite os produtos Pererekas no seu estabelecimento através do formulário:</h3>
    <div class="text-center">
      <div class="cta-wrap subscribe-elt2 ">
        <div class="container">
          <div class="col-md-12">








            <form class="intro-newsletter" action="php/mail.php" method="post" style="margin-top: -80px !important;">
              <div class="row">


                <div class="col-md-8">
                  <input placeholder="Nome Completo *" name="nome" id="nome" type="text" required>

                  <input placeholder="Nome da Empresa *" name="empresa" id="empresa" type="text" required>

                  <input placeholder="E-mail *" name="email" id="email" type="email" required>

                  <input placeholder="(DDD) Telefone *" name="telefone" id="telefone" type="tel" required> 

                  <input placeholder="Cidade *" name="cidade" id="cidade" type="text" required>

                  <select name="estado" style="margin-bottom: 20px" required>
                    <option value="">Estado</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                  </select>
                </div>



                <div class="col-md-4">

                  <div class="g-recaptcha" data-sitekey="6LdnP1AfAAAAAKZd8pOXjOv7xu0V9770ktqp4kBc"></div> 

                  <br>

                  <button class="btn btn-primary btn-block" type="submit">Enviar</button>

                </div>

              </div>

            </form>



          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Fim de Formulário -->




<div class="container">
  <div class="row"></div>
</div>
<div class="space100"></div>
<div class="space80"></div>