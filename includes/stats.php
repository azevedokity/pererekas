<!-- Stats -->
<div id="stats2" class="bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="stats2-info">
                    <i class="icon-piggy"></i>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="stats2-info">
                    <i class="icon-photo"></i>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="stats2-info">
                    <i class="icon-rocket"></i>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="stats2-info">
                    <i class="icon-world"></i>
                </div>
            </div>
        </div>
    </div>
</div>