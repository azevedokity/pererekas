<?php 

  // Mudar imagens dos slides direto aqui
  define('IMG_1', 'images/features/amor_01.png');
  define('IMG_2', 'images/features/amor_02.png');
  // define('IMG_3', 'images/features/amor_03.png'); 

?>

<!-- About Us -->
<div id="about" class="container">

  <!-- About Section 1 -->
  <div class="intro intro-about2">
    <div class="container">
      <div class="row center-content">

        <div class="col-md-7 ">
          <!-- <img src="images/features/nossahistoria.jpg" class="pull-left img-responsive" alt="imagem" /> -->

          <!-- carousel -->
          <div id="myCarousel1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel1" data-slide-to="1"></li>
              <!-- <li data-target="#myCarousel1" data-slide-to="2"></li> -->
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?= IMG_1 ?>" alt="Pererekas">
              </div>

              <div class="item">
                <img src="<?= IMG_2 ?>" alt="Pererekas">
              </div>

<!--               <div class="item">
                <img src="<?= IMG_3 ?>" alt="Pererekas">
              </div> -->
            </div>
            <!-- end wrappes for slides -->



            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
              <i class="fas fa-chevron-left seta" ></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel1" data-slide="next">
              <i class="fas fa-chevron-right seta"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <!-- end carousel -->
        </div>

        <div class="col-md-5">
          <h3>Um pedacinho da nossa história:</h3>
          <p>Projetada pelos irmãos Anderle, a empresa iniciou seus trabalhos em Caxias do Sul, no começo dos anos 2000. O primeiro lançamento foram as pipocas doces, seu maior sucesso, lançadas em seu sabor natural, ganhou novas versões de chocolate, morango e tutti frutti, permanecendo apenas o último até hoje nos mercados. Ao longo dos anos, outros produtos da marca foram inseridos no mercado, como o salgadinho de milho que pode ser encontrado nos sabores de queijo, parmesão, requeijão, cebola e salsa, churrasco e pizza, além da pipoca doce canjicada natural e o salgadinho de trigo sabor bacon.
          Com quase duas décadas de profissionalismo, a Perereka's possui duas unidades fabris, em Caxias do Sul e Sapucaia do Sul, além de empresas parceiras. Diariamente, conquistam os consumidores com seu sabor e crocância inconfundível.</p>
          <a target="_blank" href="https://www.facebook.com/pererekasoficial" class="btn btn-lg btn-primary">Acesse nosso Facebook <i class="icon-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>

  <!-- About Section 2 -->
  <div class="intro intro-about2">
    <div class="container">
      <div class="row center-content">
        <div class="col-md-5">
          <h3>O amor por aquilo que fazemos é o que nos leva mais longe.</h3>

          <ul class="list">
            <li><i class="fa fa-check"></i> Sabor.</li>
            <li><i class="fa fa-check"></i> Qualidade.</li>
            <li><i class="fa fa-check"></i> Tradição.</li>
          </ul>

          <div class="space30"></div>
          <a target="_blank" href="https://www.instagram.com/pererekasnoinsta/" class="btn btn-lg btn-primary" style="margin-bottom: 20px;">Acesse nosso Instagram <i class="icon-arrow-right"></i></a>
        </div>


        <div class="col-md-7">
          <!-- carousel -->
          <div id="myCarousel2" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel2" data-slide-to="1"></li>
              <!-- <li data-target="#myCarousel2" data-slide-to="2"></li> -->
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?= IMG_1 ?>" alt="Pererekas">
              </div>

              <div class="item">
                <img src="<?= IMG_2 ?>" alt="Pererekas">
              </div>

<!--               <div class="item">
                <img src="<?= IMG_3 ?>" alt="Pererekas">
              </div> -->
            </div>
            <!-- end wrappes for slides -->



            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
              <i class="fas fa-chevron-left seta" ></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel2" data-slide="next">
              <i class="fas fa-chevron-right seta"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <!-- end carousel -->



          <!-- imagem antiga -->
<!--           <img src="images/features/2.png" class="pull-left img-responsive" alt="imagem" /> -->
          <!-- imagem antiga -->


        </div>


      </div>
    </div>
  </div>
</div>
<div class="space80"></div>
<div class="video-action">
  <div class="row text-center">
    <div class="intro-video-pop"></div>
  </div>
</div>