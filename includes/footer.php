

        <!-- FOOTER -->
        <footer class="footer2" id="footer2" style="margin-top: -150px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 footerP text-center">
                      <h3> Siga nossas redes e fique por dentro de todas as novidades Pererekas®️.</h3> 
                    </div>
                    <div class="footer-social space30 text-center">
                        <a target="_blank" href="https://www.facebook.com/pererekasoficial" ><i class="fab fa-facebook-square"></i></a>
                        <a target="_blank" href="https://www.instagram.com/pererekasnoinsta/"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Copyright -->
        <div class="footer-copy">
            <div class="container">
                &copy; 2021 Pererekas. Todos os Direitos Reservados / Desenvolvido por Orange Studio 360
            </div>
        </div>
    </div>

<!-- JAVASCRIPT =============================-->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vendors/slick/slick.min.js"></script>
<script src="js/vendors/jquery.easing.min.js"></script>
<script src="js/vendors/stellar.js"></script>
<script src="js/vendors/isotope/isotope.pkgd.js"></script>
<script src="js/vendors/swipebox/js/jquery.swipebox.min.js"></script>
<script src="js/main.js"></script>
<script src="js/vendors/mc/jquery.ketchup.all.min.js"></script>
<script src="js/vendors/mc/main.js"></script>
</body>

</html>