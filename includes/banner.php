<?php 

  define('IMAGEM_1', 'images/bg/slide_01new.jpg');
  define('IMAGEM_2', 'images/bg/slide_02new.jpg');
  define('IMAGEM_3', 'images/bg/slide_03new.jpg');
  define('TITULO', 'Pererekas');
  define('TEXTO', 'A Melhor Hora do Seu Dia!');  

?>


<!-- Banner Principal -->
<div class="intro">

    <!-- carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?= IMAGEM_1 ?>" alt="Pererekas">
        </div>

        <div class="item">
          <img src="<?= IMAGEM_2 ?>" alt="Pererekas">
        </div>

        <div class="item">
          <img src="<?= IMAGEM_3 ?>" alt="Pererekas">
        </div>
      </div>
      <!-- end wrappes for slides -->



      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <i class="fas fa-chevron-left seta" ></i>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <i class="fas fa-chevron-right seta"></i>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <!-- end carousel -->


  <div class="container container-banner">
    <div class="row center-content">
      <div class="col-md-5 col-sm-6">
        <h3 class="pererekas-titulo-banner"><?= TITULO ?></h3>
        <p class="pererekas-text-banner"><?= TEXTO ?> </p>
        <div class="space30"></div>
        <div class="dual-btn">
          <a href="#explore" class="btn btn-lg btn-primary page-scroll">Produtos <span class="fa fa-shopping-basket"></span></a> &nbsp; &nbsp;
          <a href="#about" class="btn btn-lg btn-default btn-border page-scroll">História <i class=" ilmosys-arrow-right"></i></a>
        </div>

      </div>
      <div class="col-md-7 col-sm-6 no-padding"></div>
    </div>
  </div>
  <!-- end container-banner -->

</div>