// tira todas as classes ativas da navbar
function limpa() {
    document.getElementById('inicio').classList.remove("link-ativo");
    document.getElementById('sobre').classList.remove("link-ativo");
    document.getElementById('produtos').classList.remove("link-ativo");
    document.getElementById('peca-agora').classList.remove("link-ativo");       
}

function ativa(link) {
    limpa();
    document.getElementById(link).classList.add("link-ativo");
}

// página carregada
$(document).ready(function() {

    // ativa as classes ao clicar
    $("#inicio").click(function() {
        ativa('inicio');
    });

    $("#sobre").click(function() {
        ativa('sobre');
    });

    $("#produtos").click(function() {
        ativa('produtos');
    });

    $("#peca-agora").click(function() {
        ativa('peca-agora');
    });

});