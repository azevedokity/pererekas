<?php 

// incluir a funcionalidade do recaptcha
require_once "recaptchalib.php";

// definir a chave secreta
$secret = "6LdnP1AfAAAAAL0zoPmgT1WMIGYbwv7UoypvYcRQ";

// verificar a chave secreta
$response = null;
$reCaptcha = new ReCaptcha($secret);

if ($_POST["g-recaptcha-response"]) {
    $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
}

// deu tudo certo?
if ($response != null && $response->success) {
    

    if (!isset($_POST['estado'])) {
        die("<script>window.location.href = '../index.php'</script>");
    }

    $nome = $_POST['nome'];
    $empresa = $_POST['empresa'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $cidade = $_POST['cidade'];
    $estado = $_POST['estado'];

    ob_start();

    date_default_timezone_set('Etc/UTC');

    require 'phpmailer/PHPMailerAutoload.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;

    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';

    //Set the hostname of the mail server
    $mail->Host = 'smtp.pererekas.com.br'; 
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    // Número da porta SMTP - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    // Sitema de criptografia
    $mail->SMTPSecure = false;

    // Autenticação SMTP
    $mail->SMTPAuth = true;

    // Define se, por padrão, será utilizado TLS - Mantenha o valor "false"
    $mail->SMTPAutoTLS = false; 

    // email remetente // COLOCAR UM EMAIL PARA ENVIAR DA EMPRESA
    $mail->Username = "nao_responda@pererekas.com.br";

    // Senha do email remetente // COLOCAR A SENHA DESSE EMAIL DA EMPRESA
    $mail->Password = "iDw#37zl";

    // Email remetente
    $mail->setFrom('nao_responda@pererekas.com.br', utf8_decode("Inovatech Pererekas"));

    //Set an alternative reply-to address
    $mail->addReplyTo('nao_responda@pererekas.com.br', utf8_decode("Inovatech Pererekas"));

    ///////////////////////////  Email destinatário /////////////////////////////
    $mail->addAddress('contato@pererekas.com.br', 'Pererekas'); // tiago lemes palhano

    // escreve a mensagem a ser mostrada no email
    $mensagem = "<h2>Nome: " . $nome . "</h2>" .
                "<h3>Empresa: " . $empresa . "</h3>" .
                "<h3>Email: " . $email . "</h3>" .
                "<h3>Telefone: " . $telefone . "</h3>" .
                "<h3>Cidade: " . $cidade . "</h3>" .
                "<h3>Estado: " . $estado . "</h3>" .
                "<h3>" . "<i>Inovatech informa</i>, mensagem via site Pererekas:" . "</h3>" .
                "<p>Confira os dados de um possível cliente para revender os produtos Pererekas e entre em contato.</p>";

    $mensagem = utf8_decode($mensagem);

    // assunto do email
    $mail->Subject = utf8_decode('Inovatech informa, você tem uma nova mensagem do site Pererekas:');

    // Mensagem exibida no email do destinatário
    $mail->msgHTML($mensagem);

    //Replace the plain text body with one created manually
    $mail->AltBody = 'This is a plain-text message body';

    // send the message, check for errors
    if (!$mail->send()) {
        // echo "Mailer Error: " . $mail->ErrorInfo;
        die("<script>alert('Não fo possível enviar o email.'); window.location.href = '..'</script>");
    } 
    else{
        die("<script>alert('Email enviado com sucesso.'); window.location.href = '..'</script>");
    }
}


// não preencheu o recaptcha
else {
    ?>
    <script>alert('Preencha o recaptcha.')</script>
    <!-- <script>window.location.href = "/"</script> -->
    <script>history.go(-1);</script>
    <?php
}